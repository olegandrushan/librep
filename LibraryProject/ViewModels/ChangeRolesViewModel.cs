﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryProject.ViewModels
{
    public class ChangeRolesViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }

        public bool isAdmin { get; set; }
        public bool isLibrarian { get; set; }

        public ICollection<string> RoleNames { get; set; }
    }
}