﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryProject.ViewModels
{
    public class UsersListViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }

        public ICollection<string> RoleNames { get; set; }
    }
}