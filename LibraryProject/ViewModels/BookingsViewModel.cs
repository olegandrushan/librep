﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryProject.ViewModels
{
    public class BookingsViewModel
    {
        public string UserName { get; set; }

        public ICollection<BookingViewModel> Bookings { get; set; }
    }
}