﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryProject.ViewModels
{
    public class BookingViewModel
    {
        public DateTime BookingTime { get; set; }

        public string BookName { get; set; }
        public string GenreName { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }

    }
}