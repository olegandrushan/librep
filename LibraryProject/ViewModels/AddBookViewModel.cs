﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryProject.Models
{
    public class AddBookViewModel
    {
        public int BookId { get; set; }
        public string Name { get; set; }


        public bool isNewAuthor { get; set; }
        public int AuthorId { get; set; }
        public IEnumerable<SelectListItem> Authors { get; set; }
        public string AuthorName { get; set; }


        public bool isNewGenre { get; set; }
        public int GenreId { get; set; }
        public IEnumerable<SelectListItem> Genres { get; set; }
        public string GenreName { get; set; }

        public bool isNewPublisher { get; set; }
        public int PublisherId { get; set; }
        public IEnumerable<SelectListItem> Publishers { get; set; }
        public string PublisherName { get; set; }
    }
}