﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace LibraryProject.Models
{
    public class AddAuthorViewModel
    {
        public int AuthorId;
        public string AuthorName;
    }
}
