﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LibraryProject.Models;

namespace LibraryProject.Repositories
{
    public class PostgreBookRepository : IRepository<Book>
    {
        private ApplicationDbContext db;

        public PostgreBookRepository(ApplicationDbContext context)
        {
            db = context;
        }

        public Book GetItem(int id)
        {
            return db.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher).SingleOrDefault(b => b.Id == id);
        }

        public IEnumerable<Book> GetItemList()
        {
            return db.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher);
        }

        public IEnumerable<Book> GetFilteredList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null)
        {
            IQueryable<Book> books = db.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher);

            if (AuthorId!= null && AuthorId!= 0)
            {
                books = books.Where(b => b.AuthorId == AuthorId);
            }
            if(GenreId!= null && GenreId != 0)
            {
                books = books.Where(b => b.GenreId == GenreId);
            }
            if (PublisherId!= null && GenreId != 0)
            {
                books = books.Where(b => b.PublisherId == PublisherId);
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(b => b.Name.Contains(searchString));
            }

            return books;
        }

        public void Create(Book item)
        {
            db.Books.Add(item);
        }

        public void Update(Book item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Book book = db.Books.Find(id);
            if (book != null)
            {
                db.Books.Remove(book);
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        
        // Что дальше происходит я пока не понял

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }






    }
}