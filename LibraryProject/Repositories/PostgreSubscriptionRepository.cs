﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using LibraryProject.Models;

namespace LibraryProject.Repositories
{
    public class PostgreSubscriptionRepository : IRepository<Subscription>
    {
        private ApplicationDbContext db;

        public PostgreSubscriptionRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public void Create(Subscription item)
        {
            db.Subscriptions.Add(item);
        }

        public void Delete(int id)
        {
            var subscription = db.Subscriptions.Find(id);
            if (subscription != null)
            {
                db.Subscriptions.Remove(subscription);
            }
        }

        public Subscription GetItem(int id)
        {
            return db.Subscriptions.Find(id);
        }

        public IEnumerable<Subscription> GetItemList()
        {
            return db.Subscriptions;
        }

        public IEnumerable<Subscription> GetItemList(int BookId)
        {
            return db.Subscriptions.Include(s=>s.Book).Include(s=>s.User).Where(s => s.BookId == BookId);
        }

        public void Update(Subscription item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }
    }
}