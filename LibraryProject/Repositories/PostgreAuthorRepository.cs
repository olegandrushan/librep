﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LibraryProject.Models;

namespace LibraryProject.Repositories
{
    public class PostgreAuthorRepository : IRepository<Author>
    {
        private ApplicationDbContext db;

        public PostgreAuthorRepository(ApplicationDbContext context)
        {
            db = context;
        }

        public void Create(Author item)
        {
            db.Authors.Add(item);
        }

        public void Delete(int id)
        {
            Author author = db.Authors.Find(id);
            if (author != null)
            {
                db.Authors.Remove(author);
            }
        }

        public Author GetItem(int id)
        {
            return db.Authors.Find(id);
        }

        public IEnumerable<Author> GetItemList()
        {
            return db.Authors;
        }

        public Author GetAuthorWithBooks(int id)
        {
            return db.Authors.Include(a => a.Books).FirstOrDefault(a => a.AuthorId == id);
        }

        

        public void Update(Author item)
        {
            db.Entry(item).State = EntityState.Modified;
        }


       
    }
}