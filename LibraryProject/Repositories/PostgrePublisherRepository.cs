﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryProject.Repositories;
using LibraryProject.Models;
using System.Data.Entity;

namespace LibraryProject.Repositories
{
    public class PostgrePublisherRepository : IRepository<Publisher>
    {
        private ApplicationDbContext db;

        public PostgrePublisherRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public void Create(Publisher item)
        {
            db.Publishers.Add(item);
        }

        public void Delete(int id)
        {
            Publisher publisher = db.Publishers.Find(id);
            if (publisher != null)
            {
                db.Publishers.Remove(publisher);
            }
            
        }

        public Publisher GetItem(int id)
        {
            return db.Publishers.Find(id);
        }

        public IEnumerable<Publisher> GetItemList()
        {
            return db.Publishers;
        } 

        public void Update(Publisher item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}