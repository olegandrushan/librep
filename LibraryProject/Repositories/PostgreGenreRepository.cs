﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryProject.Models;
using System.Data.Entity;

namespace LibraryProject.Repositories
{
    public class PostgreGenreRepository : IRepository<Genre>
    {
        private ApplicationDbContext db;
        public PostgreGenreRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public void Create(Genre item)
        {
            db.Genres.Add(item);
        }

        public void Delete(int id)
        {
            Genre genre = db.Genres.Find(id);
            if (genre != null)
            {
                db.Genres.Remove(genre);
            }
        }

        public Genre GetItem(int id)
        {
            return db.Genres.Find(id);
        }

        public IEnumerable<Genre> GetItemList()
        {
            return db.Genres;
        }

        public void Update(Genre item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}