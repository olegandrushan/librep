﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LibraryProject.Models;

namespace LibraryProject.Repositories
{
    public class PostgreBookingRepository : IRepository<Booking>
    {
        private ApplicationDbContext db;

        public PostgreBookingRepository(ApplicationDbContext context)
        {
            db = context;
        }

        public void Create(Booking item)
        {
            db.Bookings.Add(item);
        }

        public void Delete(int id)
        {
            Booking booking = db.Bookings.Find(id);
            if (booking != null)
            {
                db.Bookings.Remove(booking);
            }
        }

        public Booking GetItem(int id)
        {
            return db.Bookings.Find(id);
        }

        public IEnumerable<Booking> GetItemList()
        {
            return db.Bookings;
        }

        public IEnumerable<Booking> GetReservationsList()
        {
            return db.Bookings.Where(b => b.BookingTime != null && b.RecievmentTime == null);
        }

        public IEnumerable<Booking> GetItemListEx()
        {
            return db.Bookings.Include(b => b.User).Include(b=>b.Book);
        }

        public void Update(Booking item)
        {
          db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }
    }
}