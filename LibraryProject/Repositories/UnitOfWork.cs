﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryProject.Models;
using Microsoft.AspNet.Identity;
using PostgreSQL.AspNet.Identity.EntityFramework;

namespace LibraryProject.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager userManager;
        private RoleManager<IdentityRole> roleManager;

        private PostgreBookRepository postgreBookRepository;
        private PostgreAuthorRepository postgreAuthorRepository;
        private PostgreGenreRepository postgreGenreRepository;
        private PostgrePublisherRepository PostgrePublisherRepository;
        private PostgreBookingRepository PostgreBookingRepository;
        private PostgreSubscriptionRepository PostgreSubscriptionRepository;
        

        public UnitOfWork()
        {
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
         
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
        }


        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public RoleManager<IdentityRole> RoleManager
        {
            get { return roleManager; }
        }
        

       

        public PostgreAuthorRepository Authors
        {
            get
            {
                if (postgreAuthorRepository == null)
                    postgreAuthorRepository = new PostgreAuthorRepository(db);
                return postgreAuthorRepository;
            }
        }

        public PostgreGenreRepository Genres
        {
            get
            {
                if (postgreGenreRepository == null)
                    postgreGenreRepository = new PostgreGenreRepository(db);
                return postgreGenreRepository;
            }
        }

        public PostgreBookRepository Books
        {
            get
            {
                if (postgreBookRepository == null)
                    postgreBookRepository = new PostgreBookRepository(db);
                return postgreBookRepository;
            }
        }

        public PostgrePublisherRepository Publishers
        {
            get
            {
                if (PostgrePublisherRepository == null)
                    PostgrePublisherRepository = new PostgrePublisherRepository(db);
                return PostgrePublisherRepository;
            }
        }

        public PostgreBookingRepository Bookings
        {
            get
            {
                if (PostgreBookingRepository == null)
                    PostgreBookingRepository = new PostgreBookingRepository(db);
                return PostgreBookingRepository;
            }
        }

        public PostgreSubscriptionRepository Subscriptions
        {
            get
            {
                if (PostgreSubscriptionRepository == null)
                    PostgreSubscriptionRepository = new PostgreSubscriptionRepository(db);
                return PostgreSubscriptionRepository;
            }
        }



        public void Save()
        {
            db.SaveChanges();
        }


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}