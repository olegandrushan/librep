﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using LibraryProject.Models;
using LibraryProject.Repositories;

namespace LibraryProject.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository<Book>>().To<PostgreBookRepository>();
            Bind<IRepository<Author>>().To<PostgreAuthorRepository>();
            Bind<IRepository<Genre>>().To<PostgreGenreRepository>();
            Bind<IRepository<Publisher>>().To<PostgrePublisherRepository>();            
        }
    }
}