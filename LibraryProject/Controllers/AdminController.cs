﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryProject.Models;
using LibraryProject.Repositories;
using LibraryProject.ViewModels;
using Microsoft.AspNet.Identity;

namespace LibraryProject.Controllers
{
    public class AdminController : Controller
    {
        UnitOfWork unitOfWork;


        public AdminController()
        {
            unitOfWork = new UnitOfWork();
        }
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult UsersList()
        {
            ICollection<ApplicationUser> users = unitOfWork.UserManager.Users.ToList();

            //IEnumerable<UsersListViewModel> usersList = 

            List<UsersListViewModel> usersList = new List<UsersListViewModel>();

            foreach (var user in users)
            {
                usersList.Add(new UsersListViewModel
                {
                    UserId = user.Id,
                    Email = user.Email,
                    RoleNames = unitOfWork.UserManager.GetRoles(user.Id)
                });
            }

            return View(usersList);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult ChangeRoles(string Id)
        {
            ApplicationUser user = unitOfWork.UserManager.FindById(Id);
            ChangeRolesViewModel model = new ChangeRolesViewModel
            {
                UserId = Id,
                Email = user.Email,
                isAdmin = unitOfWork.UserManager.IsInRole(user.Id, "admin"),
                isLibrarian = unitOfWork.UserManager.IsInRole(user.Id, "librarian"),
                RoleNames = unitOfWork.UserManager.GetRoles(user.Id)

            };

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult ChangeRoles(ChangeRolesViewModel model)
        {
            // Если пользователь был админом, но чекбокс админа снят
            if (unitOfWork.UserManager.IsInRole(model.UserId, "admin") && !model.isAdmin)
            {
               unitOfWork.UserManager.RemoveFromRole(model.UserId, "admin");
            }
            
            // Если пользователь не был админом, но чекбокс админа поставлен
            if (!unitOfWork.UserManager.IsInRole(model.UserId, "admin") && model.isAdmin)
            {
                unitOfWork.UserManager.AddToRole(model.UserId, "admin");
            }

            if (unitOfWork.UserManager.IsInRole(model.UserId, "librarian") && !model.isLibrarian)
            {
                unitOfWork.UserManager.RemoveFromRole(model.UserId, "librarian");
            }

            if (!unitOfWork.UserManager.IsInRole(model.UserId, "librarian") && model.isLibrarian)
            {
                unitOfWork.UserManager.AddToRole(model.UserId, "librarian");
            }

            unitOfWork.Save();


            return RedirectToAction("UsersList");
        }
    }
}