﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryProject.Models;
using LibraryProject.ViewModels;
using LibraryProject.Repositories;
using Microsoft.AspNet.Identity.Owin;
using Ninject;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.Threading;

namespace LibraryProject.Controllers
{
    public class HomeController : Controller
    {
        UnitOfWork unitOfWork;

        ApplicationDbContext db = new ApplicationDbContext();

        public HomeController()
        {
            unitOfWork = new UnitOfWork();

        }


        public ActionResult Index()
        {

            IEnumerable<Book> Books = unitOfWork.Books.GetItemList();

            return View(Books);
        }

        public ActionResult Catalog(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null)
        {
            IEnumerable<Book> books = unitOfWork.Books.GetFilteredList(AuthorId, GenreId, PublisherId, searchString);

            string UserId = User.Identity.GetUserId();
            ApplicationUser user = unitOfWork.UserManager.FindById(UserId);

            List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
        
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
     
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();
           

            BooksListViewModel model = new BooksListViewModel
            {
                Books = books,
                Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                Genres = new SelectList(genres, "GenreId", "GenreName"),
                Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                User = user

            };
                        
            return View(model);

        }

        public ActionResult Subscribe(string UserId, int BookId)
        {
            unitOfWork.Subscriptions.Create(new Subscription
            {
                UserId = UserId,
                BookId = BookId
            });

            unitOfWork.Save();

            return RedirectToAction("Catalog");
        }

        [Authorize(Roles = "admin,librarian")]
        public ActionResult BooksList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null)
        {
            IEnumerable<Book> books = unitOfWork.Books.GetFilteredList(AuthorId, GenreId, PublisherId, searchString);

            string UserId = User.Identity.GetUserId();
            ApplicationUser user = unitOfWork.UserManager.FindById(UserId);

            List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
         
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
           
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();
            

            BooksListViewModel model = new BooksListViewModel
            {
                Books = books,
                Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                Genres = new SelectList(genres, "GenreId", "GenreName"),
                Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                User = user

            };

            return View(model);
        }

        #region Добавить книгу
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult AddBook()
        {
            var model = new AddBookViewModel();
            var authors = unitOfWork.Authors.GetItemList();
            var genres = unitOfWork.Genres.GetItemList();
            var publishers = unitOfWork.Publishers.GetItemList();


            model.Authors = new SelectList(authors, "AuthorId", "AuthorName");
            model.Genres = new SelectList(genres, "GenreId", "GenreName");
            model.Publishers = new SelectList(publishers, "PublisherId", "PublisherName");

            return View(model);
        }

        [Authorize(Roles = "admin,librarian")]
        [HttpPost]
        public ActionResult AddBook(AddBookViewModel model)
        {
            Author author = new Author();
            Genre genre = new Genre();
            Publisher publisher = new Publisher();

            if (model.isNewAuthor)
            {
                author = new Author { AuthorName = model.AuthorName };
                unitOfWork.Authors.Create(author);
            }
            else
            {
                author = unitOfWork.Authors.GetItem(model.AuthorId);
            }

            if (model.isNewGenre)
            {
                genre = new Genre { GenreName = model.GenreName };
                unitOfWork.Genres.Create(genre);
            }
            else
            {
                genre = unitOfWork.Genres.GetItem(model.GenreId);
            }

            if (model.isNewPublisher)
            {
                publisher = new Publisher { PublisherName = model.PublisherName };
                unitOfWork.Publishers.Create(publisher);
            }
            else
            {
                publisher = unitOfWork.Publishers.GetItem(model.PublisherId);
            }

            Book book = new Book
            {
                Name = model.Name,
                AuthorId = author.AuthorId,
                PublisherId = publisher.PublisherId,
                GenreId = genre.GenreId
            };

            unitOfWork.Books.Create(book);
            unitOfWork.Books.Save();

            return RedirectToAction("BooksList");
        }

        #endregion

        #region Редактировать книгу
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult EditBook(int id)
        {
            Book book = unitOfWork.Books.GetItem(id);

            var authors = unitOfWork.Authors.GetItemList();
            var genres = unitOfWork.Genres.GetItemList();
            var publishers = unitOfWork.Publishers.GetItemList();

            var model = new EditBookViewModel();

            model.BookId = book.Id;
            model.Name = book.Name;
            model.AuthorId = book.AuthorId;
            model.GenreId = book.GenreId;
            model.PublisherId = book.PublisherId;



            model.Authors = new SelectList(authors, "AuthorId", "AuthorName");
            model.Genres = new SelectList(genres, "GenreId", "GenreName");
            model.Publishers = new SelectList(publishers, "PublisherId", "PublisherName");

            return View(model);

        }

        [Authorize(Roles = "admin,librarian")]
        [HttpPost]
        public ActionResult EditBook(EditBookViewModel model)
        {
            Author author = new Author();
            Genre genre = new Genre();
            Publisher publisher = new Publisher();

            if (model.isNewAuthor)
            {
                author = new Author { AuthorName = model.AuthorName };
                unitOfWork.Authors.Create(author);
            }
            else
            {
                author = unitOfWork.Authors.GetItem(model.AuthorId);
                //author = model.Book.Author;
            }

            if (model.isNewGenre)
            {
                genre = new Genre { GenreName = model.GenreName };
                unitOfWork.Genres.Create(genre);
            }
            else
            {
                genre = unitOfWork.Genres.GetItem(model.GenreId);
                //genre = model.Book.Genre;
            }

            if (model.isNewPublisher)
            {
                publisher = new Publisher { PublisherName = model.PublisherName };
                unitOfWork.Publishers.Create(publisher);
            }
            else
            {
                publisher = unitOfWork.Publishers.GetItem(model.PublisherId);
                //publisher = model.Book.Publisher;
            }

            Book book = unitOfWork.Books.GetItem(model.BookId);
            book.Name = model.Name;
            book.PublisherId = publisher.PublisherId;
            book.AuthorId = author.AuthorId;
            book.GenreId = genre.GenreId;

            unitOfWork.Books.Update(book);
            unitOfWork.Save();

            return RedirectToAction("BooksList");
        }

        #endregion 

        #region Удалить книгу
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult DeleteBook(int BookId)
        {
            Book book = unitOfWork.Books.GetItem(BookId);

            return View(book);
        }

        [Authorize(Roles = "admin,librarian")]
        [HttpPost, ActionName("DeleteBook")]
        public ActionResult DeleteBookConfirmed(int BookId)
        {
            unitOfWork.Books.Delete(BookId);
            unitOfWork.Save();



            return RedirectToAction("BooksList");

        }

        #endregion

        [Authorize]
        public ActionResult ReserveBook(int BookId, string UserId)
        {
            if (UserId == null)
            {
                RedirectToAction("Login", "Account");
            }

            Book book = unitOfWork.Books.GetItem(BookId);

            ApplicationUser user = unitOfWork.UserManager.FindById(UserId);

            Booking booking = new Booking { BookId = BookId, BookingTime = DateTime.Now, User = user };
            unitOfWork.Bookings.Create(booking);

            book.isAvailable = false;

            unitOfWork.Books.Update(book);
            unitOfWork.Save();

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public ActionResult CancellBooking(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);

            return View(booking);
        }

        [HttpPost, ActionName("CancellBooking")]
        public ActionResult CancellBookingConfirmed(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);
            Book book = unitOfWork.Books.GetItem(booking.BookId);

            book.isAvailable = true;

            unitOfWork.Books.Update(book);

            unitOfWork.Bookings.Delete(Id);

            unitOfWork.Save();

            IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

            foreach (var subscription in subscriptions)
            {

                MailAddress from = new MailAddress("bibaboomer18@gmail.com", "Библиотека");

                MailAddress to = new MailAddress(subscription.User.Email);

                MailMessage m = new MailMessage(from, to);

                m.Subject = "Книга на обновления которой Вы подписались доступна для бронирования";

                string bookname = subscription.Book.Name;

                m.Body = "<h2>Книга " + bookname + " доступна для бронирования</h2>";


                m.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential("bibaboomer18@gmail.com", "Etoneparol1998");
                smtp.EnableSsl = true;
                smtp.Send(m);
            }

            return RedirectToAction("MyBookings");
        }



        public ActionResult TestBookings(string Id)
        {

            //ApplicationUser user = unitOfWork.UserManager.FindById(Id);

            ApplicationUser user = db.Users.Include(u => u.Bookings).FirstOrDefault(u => u.Id == Id);



            return View(user);
        }

        [Authorize]
        public ActionResult MyBookings()
        {
            string UserId = User.Identity.GetUserId();

            ApplicationUser user = unitOfWork.UserManager.Users.Include(u => u.Bookings.Select(b => b.Book)).FirstOrDefault(u => u.Id == UserId);

            return View(user);
        }

        
      




            /*    public ActionResult UserBookings(string Id)
                {
                    ApplicationUser user = unitOfWork.UserManager.FindById(Id);

                    BookingsViewModel bookings = new BookingsViewModel
                    {
                        UserName = user.Email,
                        Bookings = new List<BookingViewModel>()
                    };

                    foreach (var a in user.Bookings)
                    {
                        Book book = unitOfWork.Books.GetItem(a.BookId);

                        if (book != null)
                        {
                            bookings.Bookings.Add(new BookingViewModel
                            {
                                AuthorName = book.Author.AuthorName,
                                BookName = book.Name,
                                GenreName = book.Genre.GenreName,
                                PublisherName = book.Publisher.PublisherName,
                                BookingTime = a.BookingTime
                            });
                        }
                        else {
                            bookings.Bookings.Add(new BookingViewModel
                            {
                                AuthorName = "Книга недоступна",
                                BookName = "Книга недоступна",
                                GenreName = "Книга недоступна",
                                PublisherName = "Книга недоступна",
                                BookingTime = a.BookingTime

                            }); ;
                        }

                    }

                    return View(bookings);
                }*/


        }
    }
