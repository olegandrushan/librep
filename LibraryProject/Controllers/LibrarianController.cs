﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using LibraryProject.ViewModels;
using LibraryProject.Models;
using LibraryProject.Repositories;

namespace LibraryProject.Controllers
{
    public class LibrarianController : Controller
    {

        UnitOfWork unitOfWork;

        public LibrarianController()
        {
            unitOfWork = new UnitOfWork();
        }

        // GET: Librarian
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ReservationList()
        {
            // Забронированные, но не выданные книги

            IEnumerable<Booking> bookings = unitOfWork.Bookings.GetItemListEx().Where(b => b.RecievmentTime == null && b.BookingTime != null);

            return View(bookings);
        }

        public ActionResult BookingList()
        {
            // Выданные, но не возвращенные книги

            IEnumerable<Booking> bookings = unitOfWork.Bookings.GetItemListEx().Where(b => b.ReturningTime == null && b.RecievmentTime != null);

            return View(bookings);
        }

        #region Список издателей
        [Authorize(Roles = "admin,librarian")]
        public ActionResult PublisherList()
        {
            IEnumerable<Publisher> publishers = unitOfWork.Publishers.GetItemList();
            return View(publishers);
        }
        #endregion
        #region Редактирование издателя
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult EditPublisher(int id)
        {
            Publisher publisher = unitOfWork.Publishers.GetItem(id);

            return View(publisher);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost]
        public ActionResult EditPublisher(Publisher publisher)
        {
            unitOfWork.Publishers.Update(publisher);
            unitOfWork.Save();

            return RedirectToAction("PublisherList");
        }
        #endregion
        #region Удаление издателя
        public ActionResult DeletePublisher(int id)
        {
            Publisher publisher = unitOfWork.Publishers.GetItem(id);

            return View(publisher);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost, ActionName("DeletePublisher")]
        public ActionResult DeletePublisherConfirmed(int id)
        {
            unitOfWork.Publishers.Delete(id);


            unitOfWork.Save();

            return RedirectToAction("PublisherList");
        }
        #endregion
        #region Список жанров
        [Authorize(Roles = "admin,librarian")]
        public ActionResult GenreList()
        {
            IEnumerable<Genre> genres = unitOfWork.Genres.GetItemList();
            return View(genres);
        }
        #endregion
        #region Добавить жанр
        [HttpGet]
        [Authorize(Roles = "admin,librarian")]
        public ActionResult AddGenre()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin,librarian")]
        public ActionResult AddGenre(Genre genre)
        {
            unitOfWork.Genres.Create(genre);
            unitOfWork.Save();
            return RedirectToAction("GenreList","Librarian");
        }
        #endregion
        #region Редактирование жанра
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult EditGenre(int id)
        {
            Genre genre = unitOfWork.Genres.GetItem(id);

            return View(genre);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost]
        public ActionResult EditGenre(Genre genre)
        {
            unitOfWork.Genres.Update(genre);
            unitOfWork.Save();

            return RedirectToAction("GenreList");
        }

        #endregion
        #region Удаление жанра
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult DeleteGenre(int id)
        {
            Genre genre = unitOfWork.Genres.GetItem(id);

            return View(genre);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost, ActionName("DeleteGenre")]
        public ActionResult DeleteGenreConfirmed(int id)
        {


            unitOfWork.Genres.Delete(id);


            unitOfWork.Save();

            return RedirectToAction("GenreList");
        }
        #endregion
        #region Добавить автора
        [HttpGet]
        [Authorize(Roles = "admin,librarian")]
        public ActionResult AddAuthor()
        {           
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin,librarian")]
        public ActionResult AddAuthor(Author author)
        {         
            unitOfWork.Authors.Create(author);
            unitOfWork.Save();

            return RedirectToAction("AuthorsList","Librarian");
        }
        #endregion
        #region Список авторов
        [Authorize(Roles = "admin,librarian")]
        public ActionResult AuthorsList()
        {
            IEnumerable<Author> authors = unitOfWork.Authors.GetItemList();

            return View(authors);
        }

        #endregion

        #region Редактирование автора
        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult EditAuthor(int id)
        {
            Author author = unitOfWork.Authors.GetItem(id);

            return View(author);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost]
        public ActionResult EditAuthor(Author author)
        {
            unitOfWork.Authors.Update(author);
            unitOfWork.Save();

            return RedirectToAction("AuthorsList");
        }

        #endregion

        #region Удаление автора

        [Authorize(Roles = "admin,librarian")]
        [HttpGet]
        public ActionResult DeleteAuthor(int id)
        {
            Author author = unitOfWork.Authors.GetItem(id);

            return View(author);
        }
        [Authorize(Roles = "admin,librarian")]
        [HttpPost, ActionName("DeleteAuthor")]
        public ActionResult DeleteAuthorConfirmed(int id)
        {


            unitOfWork.Authors.Delete(id);


            unitOfWork.Save();

            return RedirectToAction("AuthorsList");
        }

        #endregion

        public ActionResult AuthorBooks(int id)
        {
            Author author = unitOfWork.Authors.GetAuthorWithBooks(id);

            return View(author);
        }

        [Authorize(Roles = "admin,librarian")]
        public ActionResult GiveBook(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);
            booking.RecievmentTime = DateTime.Now;

            unitOfWork.Bookings.Update(booking);
            unitOfWork.Save();

            return RedirectToAction("BookingList");
        }
        [Authorize(Roles = "admin,librarian")]
        public ActionResult ReturnBook(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);
            booking.ReturningTime = DateTime.Now;

            Book book = unitOfWork.Books.GetItem(booking.BookId);
            book.isAvailable = true;

            unitOfWork.Bookings.Update(booking);
            unitOfWork.Books.Update(book);
            unitOfWork.Save();

            IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

            foreach (var subscription in subscriptions)
            {

                MailAddress from = new MailAddress("bibaboomer18@gmail.com", "Библиотека");

                MailAddress to = new MailAddress(subscription.User.Email);

                MailMessage m = new MailMessage(from, to);

                m.Subject = "Книга на обновления которой Вы подписались доступна для бронирования";

                string bookname = subscription.Book.Name;

                m.Body = "<h2>Книга " + bookname + " доступна для бронирования</h2>";

                m.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential("bibaboomer18@gmail.com", "Etoneparol1998");
                smtp.EnableSsl = true;
                smtp.Send(m);
            }

            return RedirectToAction("BookingList");

        }


        [Authorize(Roles = "admin,librarian")]
        public ActionResult CheckReservations()
        {
            IEnumerable<Booking> reservations = unitOfWork.Bookings.GetReservationsList().ToList();

            foreach (var reservation in reservations)
            {
                if (DateTime.Now > reservation.BookingTime.AddMinutes(1))
                {

                    Booking booking = unitOfWork.Bookings.GetItem(reservation.Id);
                    Book book = unitOfWork.Books.GetItem(booking.BookId);

                    book.isAvailable = true;

                    unitOfWork.Books.Update(book);

                    unitOfWork.Bookings.Delete(reservation.Id);

                    unitOfWork.Save();

                    IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

                    foreach (var subscription in subscriptions)
                    {

                        MailAddress from = new MailAddress("bibaboomer18@gmail.com", "Библиотека");

                        MailAddress to = new MailAddress(subscription.User.Email);

                        MailMessage m = new MailMessage(from, to);

                        m.Subject = "Книга на обновления которой Вы подписались доступна для бронирования";

                        string bookname = subscription.Book.Name;

                        m.Body = "<h2>Книга " + bookname + " доступна для бронирования</h2>" +
                                    "<p>Перейдите по ссылке, чтобы забронировать её </p>";

                        m.IsBodyHtml = true;

                        SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                        smtp.Credentials = new NetworkCredential("bibaboomer18@gmail.com", "Etoneparol1998");
                        smtp.EnableSsl = true;
                        smtp.Send(m);
                    }

                }
            }

            return RedirectToAction("ReservationList");

        }
    }
}