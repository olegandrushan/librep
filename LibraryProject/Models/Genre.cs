﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryProject.Models
{
    public class Genre
    {
        public int GenreId { get; set; }

        [Display(Name ="Жанр")]
        public string GenreName { get; set; }

        public ICollection<Book> Books { get; set; }

        public Genre()
        {
            Books = new List<Book>();
        }
    }
}