﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryProject.Models
{
    public class Booking
    {
        public int Id { get; set; }

        public DateTime BookingTime { get; set; }

        //Дата выдачи книги
        public DateTime? RecievmentTime { get; set; }

        //Дата возвращения книги
        public DateTime? ReturningTime { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }


        

    }
}