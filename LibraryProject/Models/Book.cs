﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LibraryProject.Models
{
    public class Book
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Display(Name = "Название")]

        [Required]
        public string Name { get; set; }
        [Required]
        public int GenreId { get; set; }

        public Genre Genre { get; set; }
        [Required]
        public int AuthorId { get; set; }

        public Author Author { get; set; }
        [Required]
        public int PublisherId { get; set; }

        public Publisher Publisher { get; set; }

        // Доступна ли книга?
        public bool isAvailable { get; set; }

        public Book()
        {
            isAvailable = true;
        }

    }
}