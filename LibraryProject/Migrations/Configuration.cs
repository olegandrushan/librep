namespace LibraryProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using LibraryProject.Models;
    using Microsoft.AspNet.Identity;
    using PostgreSQL.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<LibraryProject.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "LibraryProject.Models.ApplicationDbContext";
        }

        protected override void Seed(LibraryProject.Models.ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // ������� ��� ����
            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };
            var role3 = new IdentityRole { Name = "librarian" };

            // ��������� ���� � ��
            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            // ������� �������������
            var admin = new ApplicationUser { Email = "admin@gmail.com", UserName = "admin@gmail.com" };
            string password = "Etoneparol-1998";
            var result = userManager.Create(admin, password);

            // ���� �������� ������������ ������ �������
            if (result.Succeeded)
            {
                // ��������� ��� ������������ ����
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(admin.Id, role2.Name);
                userManager.AddToRole(admin.Id, role3.Name);
            }

            context.Authors.Add(new Author { AuthorId = 1, AuthorName = "��� �������" });
            context.Genres.Add(new Genre { GenreId = 1, GenreName = "������������ �����" });
            context.Publishers.Add(new Publisher { PublisherId = 1, PublisherName = "������" });
            context.Books.Add(new Book { Id = 1, Name = "����� � ���", AuthorId=1, GenreId=1, PublisherId=1 });
            

            base.Seed(context);
        }
    }
}
