﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using LibraryProject.Models;
using LibraryProject.Repositories;

namespace LibraryProject.Modules
{
    public class TimerModule: IHttpModule
    {
        static Timer timer;
        long interval = 3600000; //1 час
        static object synclock = new object();

       

        public void Init(HttpApplication app)
        {
            
            timer = new Timer(new TimerCallback(CheckReservations), null, 0, interval);
        }

        private void CheckReservations(object obj)
        {
            lock (synclock)
            {
                UnitOfWork unitOfWork = new UnitOfWork();

                IEnumerable<Booking> reservations = unitOfWork.Bookings.GetReservationsList().ToList();

                foreach (var reservation in reservations)
                {
                    if(DateTime.Now  > reservation.BookingTime.AddDays(3))
                    {
                       
                        Booking booking = unitOfWork.Bookings.GetItem(reservation.Id);
                        Book book = unitOfWork.Books.GetItem(booking.BookId);

                        book.isAvailable = true;

                        unitOfWork.Books.Update(book);

                        unitOfWork.Bookings.Delete(reservation.Id);

                        unitOfWork.Save();

                        IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

                        foreach (var subscription in subscriptions)
                        {

                            MailAddress from = new MailAddress("bibaboomer18@gmail.com", "Библиотека");

                            MailAddress to = new MailAddress(subscription.User.Email);

                            MailMessage m = new MailMessage(from, to);

                            m.Subject = "Книга на обновления которой Вы подписались доступна для бронирования";

                            string bookname = subscription.Book.Name;

                            m.Body = "<h2>Книга " + bookname + " доступна для бронирования</h2>" +
                                        "<p>Перейдите по ссылке, чтобы забронировать её </p>";

                            m.IsBodyHtml = true;

                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                            smtp.Credentials = new NetworkCredential("bibaboomer18@gmail.com", "Etoneparol1998");
                            smtp.EnableSsl = true;
                            smtp.Send(m);
                        }

                    }
                }

                
            }
        }
      
        public void Dispose()
        { }
    }
}